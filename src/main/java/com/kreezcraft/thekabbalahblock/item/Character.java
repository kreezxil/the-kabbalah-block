package com.kreezcraft.thekabbalahblock.item;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

public class Character extends Item {
    public Character(Item.Properties properties) {
        super(properties);
    }

    @Override
    public boolean isFoil(ItemStack p_41453_) {
        return true;
    }
}