package com.kreezcraft.thekabbalahblock.item;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;

public class ModCreativeModeTab {
    public static final CreativeModeTab LETTERS_TAB = new CreativeModeTab("letterstab") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ModItems.LETTER_A.get());
        }
    };
}
