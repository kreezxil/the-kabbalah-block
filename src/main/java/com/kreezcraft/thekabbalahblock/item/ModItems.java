package com.kreezcraft.thekabbalahblock.item;

import com.kreezcraft.thekabbalahblock.TheKabbalahBlock;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;


public class ModItems {
    public static final DeferredRegister<Item> ITEMS =
            DeferredRegister.create(ForgeRegistries.ITEMS, TheKabbalahBlock.MODID);

    public static final RegistryObject<Item> LETTER_A = ITEMS.register("letter_a", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_B = ITEMS.register("letter_b", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_C = ITEMS.register("letter_c", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_D = ITEMS.register("letter_d", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_E = ITEMS.register("letter_e", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_F = ITEMS.register("letter_f", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_G = ITEMS.register("letter_g", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_H = ITEMS.register("letter_h", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_I = ITEMS.register("letter_i", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_J = ITEMS.register("letter_j", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_K = ITEMS.register("letter_k", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_L = ITEMS.register("letter_l", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_M = ITEMS.register("letter_m", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_N = ITEMS.register("letter_n", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_O = ITEMS.register("letter_o", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_P = ITEMS.register("letter_p", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_Q = ITEMS.register("letter_q", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_R = ITEMS.register("letter_r", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_S = ITEMS.register("letter_s", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_T = ITEMS.register("letter_t", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_U = ITEMS.register("letter_u", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_V = ITEMS.register("letter_v", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_W = ITEMS.register("letter_w", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_X = ITEMS.register("letter_x", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_Y = ITEMS.register("letter_y", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_Z = ITEMS.register("letter_z", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));

    public static final RegistryObject<Item> LETTER_COLON = ITEMS.register("letter_colon", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static final RegistryObject<Item> LETTER_UNDERSCORE = ITEMS.register("letter_underscore", () -> new Character(new Item.Properties().tab(ModCreativeModeTab.LETTERS_TAB)));
    public static void register(IEventBus eventBus) {
        ITEMS.register(eventBus);
    }
}
